Course work Tinkoff Fintech Android
==================================


Introduction
--------
Application consisting of:

1. Authorization

2. Main screen :

  * [Tab "My course"] - Display a list of all passed and future tests.
    * [Passing the test] - Passing tests based on previous lectures before class.
  * [Tab "Profile"] - Displays information about the user with the ability to edit it.
  * [Tab "Settings"] - Contains auxiliary functionality.


Getting Started
---------------
This project uses the Gradle build system. To build this project, use the
`gradlew build` command or use "Import Project" in Android Studio.

Screenshots
-----------

![image-1](screenshots/image-1.png "Auth")
![image-2](screenshots/image-2.png "Tests list")
![image-3](screenshots/image-3.png "Profile")

Libraries Used
-----------------------
* [Foundation][0] - Components for core system capabilities.
    * [AppCompat][1] - Degrade gracefully on older versions of Android.
* [Architecture][10] - A collection of libraries that help you design robust, testable, and
  maintainable apps. Start with classes for managing your UI component lifecycle and handling data
  persistence.
    * [Room][16] - Access your app's SQLite database with in-app objects and compile-time checks.
* [UI][30] - Details on why and how to use UI Components in your apps - together or separate
    * [Animations & Transitions][31] - Move widgets and transition between screens.
    * [Fragment][34] - A basic unit of composable UI.
    * [Layout][35] - Lay out widgets using different algorithms.
* Third party
    * [Glide][90] For image loading
    * [Rxjava2][91] Async request
    * [Retrofit2][92] Http client
    * [Timber][93] For logging
    * [Stetho][94] For debug database



[0]: https://developer.android.com/jetpack/foundation/
[1]: https://developer.android.com/topic/libraries/support-library/packages#v7-appcompat
[10]: https://developer.android.com/jetpack/arch/
[16]: https://developer.android.com/topic/libraries/architecture/room
[30]: https://developer.android.com/jetpack/ui/
[31]: https://developer.android.com/training/animation/
[34]: https://developer.android.com/guide/components/fragments
[35]: https://developer.android.com/guide/topics/ui/declaring-layout
[90]: https://bumptech.github.io/glide/
[91]: https://github.com/ReactiveX/RxJava
[92]: https://square.github.io/retrofit/
[93]: https://github.com/JakeWharton/timber
[94]: https://github.com/facebook/stetho



