package ru.fintech.coursework

import android.app.Application
import com.facebook.stetho.Stetho
import ru.fintech.coursework.di.*
import timber.log.Timber

class App : Application() {

    val gson by lazy(::provideGson)
    val prefs by lazy(::providePrefs)
    val database by lazy(::provideDatabase)
    val repository by lazy(::provideRepository)
    val prefsCookie by lazy (:: providePrefsCookie)
    val remoteService by lazy(::provideRemoteService)
    val databaseService by lazy (::provideDatabaseService)

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
