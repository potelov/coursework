package ru.fintech.coursework.utils

enum class LoggedInMode constructor(val type: Int) {
    LOGGED_IN_MODE_LOGGED_OUT(0),
    LOGGED_IN_MODE_SERVER(1)
}