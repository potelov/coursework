package ru.fintech.coursework.utils.extensions

import android.app.Activity
import ru.fintech.coursework.App

val Activity.app get() = application as App


