package ru.fintech.coursework.utils.extensions

import androidx.fragment.app.Fragment
import ru.fintech.coursework.App

val Fragment.app get() = activity?.application as App
