package ru.fintech.coursework.utils

const val BASE_URL = "https://fintech.tinkoff.ru/api/"
const val FINTECH_URL = "https://fintech.tinkoff.ru"
const val DATE_FORMAT = "yyyy-MM-dd HH:mm:ss"
const val PREF_COOKIES = "PREF_COOKIES"
const val PREF_APP = "PREF_APP"
const val DB_NAME = "fintech"
