package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Test (

    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("task_type")
    @Expose
    val taskType: String,
    @SerializedName("max_score")
    @Expose
    val maxScore: String,
    @SerializedName("deadline_date")
    @Expose
    val deadlineDate: String?,
    @SerializedName("contest_info")
    @Expose
    val contestInfo: ContestInfo,
    @SerializedName("short_name")
    @Expose
    val shortName: String

) : Parcelable
