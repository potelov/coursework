package ru.fintech.coursework.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("user")
    @Expose
    val user: User,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("message")
    @Expose
    val message: String
)

@Entity
data class User(
    @PrimaryKey()
    @SerializedName("id")
    @Expose
    val id: Long,
    @SerializedName("birthday")
    @Expose
    var birthday: String? = "",
    @SerializedName("email")
    @Expose
    var email: String? = "",
    @SerializedName("first_name")
    @Expose
    var firstName: String? = "",
    @SerializedName("last_name")
    @Expose
    var lastName: String? = "",
    @SerializedName("phone_mobile")
    @Expose
    var phone: String? = "",
    @SerializedName("region")
    @Expose
    var region: String? = "",
    @SerializedName("university")
    @Expose
    var university: String? = "",
    @SerializedName("avatar")
    @Expose
    var avatar: String? = ""
)