package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HomeworksResponse(

    @SerializedName("homeworks")
    @Expose
    val homeworks: List<Homework>

)
