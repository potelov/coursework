package ru.fintech.coursework.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.fintech.coursework.data.database.dao.UserDao
import ru.fintech.coursework.data.model.User

@Database(entities = [(User::class)], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

}