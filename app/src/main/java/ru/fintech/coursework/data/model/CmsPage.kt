package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class CmsPage(

    @SerializedName("unstyled_statement")
    @Expose
    var unstyledStatement: String

) : Parcelable
