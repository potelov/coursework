package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginRequest internal constructor(
    @Expose
    @SerializedName("email") internal val email: String,
    @Expose
    @SerializedName("password") internal val password: String
)


