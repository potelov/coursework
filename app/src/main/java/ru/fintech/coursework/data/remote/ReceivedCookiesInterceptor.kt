package ru.fintech.coursework.data.remote

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import ru.fintech.coursework.data.preferences.PREF_KEY_USER_COOKIE
import ru.fintech.coursework.data.preferences.PREF_KEY_USER_TOKEN
import ru.fintech.coursework.utils.PREF_COOKIES
import java.io.IOException
import java.util.*

class ReceivedCookiesInterceptor constructor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val response = chain.proceed(chain.request())
        if (response.networkResponse()?.request()?.url().toString().contains("signin")) {
            val cookies = HashSet(response.headers("Set-Cookie"))
            val stringBuilder = StringBuilder()
            var xcsrftoken = ""
            for (cookie in cookies) {
                when {
                    cookie.contains("csrftoken") -> {
                        xcsrftoken = cookie.substringAfter("=").substringBefore(";")
                        val csrftoken = cookie.substringBefore(";")
                        stringBuilder.append(csrftoken)
                        stringBuilder.append(";")
                        stringBuilder.append(" ")
                    }
                    cookie.contains("anygen") -> {
                        val anygen = cookie.substringBefore(";")
                        stringBuilder.append(anygen)
                        stringBuilder.append(";")
                        stringBuilder.append(" ")
                    }
                }
            }
            if (stringBuilder.isNotEmpty()) {
                val cookie = stringBuilder.dropLast(2).toString()
                val prefs = context.getSharedPreferences(PREF_COOKIES, Context.MODE_PRIVATE)
                prefs.edit().putString(PREF_KEY_USER_COOKIE, cookie).apply()
                prefs.edit().putString(PREF_KEY_USER_TOKEN, xcsrftoken).apply()
            }
        }
        return response
    }
}