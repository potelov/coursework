package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ContestStatus(
    @SerializedName("time_left")
    @Expose
    var timeLeft: String?,
    @SerializedName("status")
    @Expose
    var status: String?
) : Parcelable
