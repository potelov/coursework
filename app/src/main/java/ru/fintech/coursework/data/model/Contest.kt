package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Contest(
    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("rel_titles")
    @Expose
    val relTitles: String?,
    @SerializedName("time_left")
    @Expose
    val timeLeft: Int?,
    @SerializedName("duration")
    @Expose
    val duration: Int?
)
