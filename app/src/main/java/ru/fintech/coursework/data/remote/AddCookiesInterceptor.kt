package ru.fintech.coursework.data.remote

import android.content.Context
import okhttp3.Interceptor
import okhttp3.Response
import ru.fintech.coursework.data.preferences.PREF_KEY_USER_COOKIE
import ru.fintech.coursework.utils.PREF_COOKIES
import java.io.IOException

class AddCookiesInterceptor constructor(private val context: Context) : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        if (!chain.request().url().toString().contains("signin")) {
            val prefs = context.getSharedPreferences(PREF_COOKIES, Context.MODE_PRIVATE)
            val cookie = prefs.getString(PREF_KEY_USER_COOKIE, "")
            builder.addHeader("Cookie", cookie!!)
        }
        return chain.proceed(builder.build())
    }
}