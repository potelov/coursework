package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ContestInfo(
    @SerializedName("contest_status")
    @Expose
    val contestStatus: ContestStatus?,
    @SerializedName("contest_url")
    @Expose
    val contestUrl: String
) : Parcelable
