package ru.fintech.coursework.data.preferences

import android.content.Context
import ru.fintech.coursework.utils.PREF_COOKIES

const val PREF_KEY_USER_COOKIE = "PREF_KEY_USER_COOKIE"
const val PREF_KEY_USER_TOKEN = "PREF_KEY_USER_TOKEN"

interface AppPreferencesCookie {
    fun setCookie(cookie: String?)
    fun getCookie(): String?
    fun setToken(token: String?)
    fun getToken(): String?
    fun clearCookies()
}

class AppPreferencesCookieImpl(context: Context) : AppPreferencesCookie {

    private val prefs = context.getSharedPreferences(PREF_COOKIES, Context.MODE_PRIVATE)

    override fun setCookie(cookie: String?) {
        prefs.edit().putString(PREF_KEY_USER_COOKIE, cookie).apply()
    }

    override fun getCookie(): String? {
        return prefs.getString(PREF_KEY_USER_COOKIE, "")
    }

    override fun setToken(token: String?) {
        prefs.edit().putString(PREF_KEY_USER_TOKEN, token).apply()
    }

    override fun getToken(): String? {
        return prefs.getString(PREF_KEY_USER_TOKEN, "")
    }

    override fun clearCookies() {
        prefs.edit().remove(PREF_KEY_USER_TOKEN).apply()
        prefs.edit().remove(PREF_KEY_USER_COOKIE).apply()
    }
}