package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Homework(

    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("title")
    @Expose
    val title: String,
    @SerializedName("tasks")
    @Expose
    val tasks: List<Tasks>

)
