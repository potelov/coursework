package ru.fintech.coursework.data.database

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import ru.fintech.coursework.data.model.User
import ru.fintech.coursework.data.model.UserResponse

interface AppDatabaseService {
    fun clearUserFromDatabase()
    fun getUser(): Observable<User>
    fun saveUser(userResponse: UserResponse): Observable<UserResponse>
}

class AppDatabaseImpl constructor(private val database: AppDatabase) : AppDatabaseService {

    override fun getUser(): Observable<User> {
        return database.userDao().getUser()
    }

    override fun saveUser(userResponse: UserResponse): Observable<UserResponse> {
        return Observable.fromCallable {
            database.userDao().insert(userResponse.user)
            userResponse
        }
    }

    override fun clearUserFromDatabase() {
        Completable.fromAction { database.clearAllTables() }
            .subscribeOn(Schedulers.io())
            .subscribe()
    }
}