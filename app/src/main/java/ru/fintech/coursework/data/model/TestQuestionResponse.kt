package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class TestQuestionResponse(

    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("position")
    @Expose
    var position: Int,
    @SerializedName("problem")
    @Expose
    var problem: Problem,
    @SerializedName("status")
    @Expose
    var status: String?,
    @SerializedName("attempts_left")
    @Expose
    var attemptsLeft: Int,
    @SerializedName("report_attempts_left")
    @Expose
    var reportAttemptsLeft: Int,
    @SerializedName("last_submission")
    @Expose
    var lastSubmission: LastSubmission?

) : Parcelable
