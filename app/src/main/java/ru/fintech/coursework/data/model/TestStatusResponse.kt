package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TestStatusResponse(

    @SerializedName("contest")
    @Expose
    val contest: Contest?,
    @SerializedName("problems")
    @Expose
    val problems: List<String>?

)
