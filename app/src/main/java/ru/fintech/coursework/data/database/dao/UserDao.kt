package ru.fintech.coursework.data.database.dao

import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Observable
import ru.fintech.coursework.data.model.User

@Dao
abstract class UserDao : BaseDao<User> {

    @Query("SELECT * FROM user")
    abstract fun getUser(): Observable<User>
}