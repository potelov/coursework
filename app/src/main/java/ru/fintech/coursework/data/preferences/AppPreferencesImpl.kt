package ru.fintech.coursework.data.preferences

import android.content.Context
import ru.fintech.coursework.utils.LoggedInMode
import ru.fintech.coursework.utils.PREF_APP

private const val PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE"

interface AppPreferences {
    fun setCurrentUserLoggedInMode(mode: LoggedInMode)
    fun getCurrentUserLoggedInMode(): Int
    fun isAuthenticated(): Boolean
}

class AppPreferencesImpl(context: Context) : AppPreferences {

    private val prefs = context.getSharedPreferences(PREF_APP, Context.MODE_PRIVATE)

    override fun setCurrentUserLoggedInMode(mode: LoggedInMode) {
        prefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode.type).apply()
    }

    override fun getCurrentUserLoggedInMode(): Int {
        return prefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE, LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type)
    }

    override fun isAuthenticated(): Boolean {
        return getCurrentUserLoggedInMode() != LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT.type
    }
}