package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TestRequest (
    @Expose
    @SerializedName("answer") val answer: String,
    @Expose
    @SerializedName("file") var file:  String? = null,
    @Expose
    @SerializedName("language") var language:  Int = 1
)
