
package ru.fintech.coursework.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//todo change to kotlin
public class LastSubmission implements Parcelable {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("programming_language")
    @Expose
    private String programmingLanguage;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("report")
    @Expose
    private String report;
    @SerializedName("problem_name")
    @Expose
    private String problemName;
    @SerializedName("problem_id")
    @Expose
    private int problemId;

    protected LastSubmission(Parcel in) {
        id = in.readInt();
        status = in.readString();
        createdAt = in.readString();
        programmingLanguage = in.readString();
        file = in.readString();
        report = in.readString();
        problemName = in.readString();
        problemId = in.readInt();
    }

    public static final Creator<LastSubmission> CREATOR = new Creator<LastSubmission>() {
        @Override
        public LastSubmission createFromParcel(Parcel in) {
            return new LastSubmission(in);
        }

        @Override
        public LastSubmission[] newArray(int size) {
            return new LastSubmission[size];
        }
    };

    public LastSubmission() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        this.programmingLanguage = programmingLanguage;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getProblemName() {
        return problemName;
    }

    public void setProblemName(String problemName) {
        this.problemName = problemName;
    }

    public int getProblemId() {
        return problemId;
    }

    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(status);
        dest.writeString(createdAt);
        dest.writeString(programmingLanguage);
        dest.writeString(file);
        dest.writeString(report);
        dest.writeString(problemName);
        dest.writeInt(problemId);
    }

    public void setSingleFile(int position, int size) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            if (i == position) {
                sb.append("1");
            } else {
                sb.append("0");
            }
        }
        file = sb.toString();
    }

    public void setMultipleFile(int position, int size, boolean checked) {
        StringBuilder sb = new StringBuilder();

        if (file == null) {
            for (int i = 0; i < size; i++) {
                sb.append("0");
            }
        } else {
            sb.append(file);
        }

        for (int i = 0; i < size; i++) {
            if (i == position) {
                if (checked) {
                    sb.setCharAt(i, '1');
                } else {
                    sb.setCharAt(i, '0');
                }
                break;
            }
        }
        file = sb.toString();
    }
}
