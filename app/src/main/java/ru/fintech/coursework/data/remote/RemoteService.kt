package ru.fintech.coursework.data.remote

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*
import ru.fintech.coursework.data.model.*

private const val HEADER_REFERER = "Referer: https://fintech.tinkoff.ru/"
private const val HEADER_XCSRFTOKEN = "X-CSRFToken"

interface RemoteService {

    @POST("signin")
    fun doLoginUserApiCall(@Body request: LoginRequest): Single<User>

    @Headers(HEADER_REFERER)
    @POST("signout")
    fun doLogoutUserApiCall(@Header(HEADER_XCSRFTOKEN) xcsrftoken: String?): Single<Response<Void>>

    @GET("user")
    fun getUserApiCall(): Observable<UserResponse>

    @Headers(HEADER_REFERER)
    @PUT("register_user")
    fun updateUserApiCall(@Body user: User, @Header(HEADER_XCSRFTOKEN) xcsrftoken: String?): Single<String>

    @GET("course/android_fall2018/homeworks")
    fun getHomeworksApiCall(): Single<HomeworksResponse>

    @GET("contest/{id}/status")
    fun getStatusTestApiCall(@Path("id") id: String): Single<TestStatusResponse>

    @GET("contest/{id}/problems")
    fun getQuestionsApiCall(@Path("id") id: String): Single<List<TestQuestionResponse>>

    @GET("contest/{id}/problem/{problemId}")
    fun getQuestionApiCall(@Path("id") id: String, @Path("problemId") problemId: String): Single<TestQuestionResponse>

    @Headers(HEADER_REFERER)
    @POST("contest/{lecture_id}/problem/{question_id}")
    fun doSendAnswerApiCall(@Path("lecture_id") lectureId: String,
                            @Path("question_id") questionId: String,
                            @Body testRequest: TestRequest,
                            @Header(HEADER_XCSRFTOKEN) xcsrftoken: String?): Single<AnswerResponse>

}