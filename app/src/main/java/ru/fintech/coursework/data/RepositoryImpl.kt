package ru.fintech.coursework.data

import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Response
import ru.fintech.coursework.data.database.AppDatabaseService
import ru.fintech.coursework.data.model.*
import ru.fintech.coursework.data.preferences.AppPreferences
import ru.fintech.coursework.data.preferences.AppPreferencesCookie
import ru.fintech.coursework.data.remote.RemoteService
import ru.fintech.coursework.utils.LoggedInMode

interface Repository : RemoteService, AppPreferences, AppDatabaseService, AppPreferencesCookie {
    fun setUserAsLoggedOut()
    fun getTests(): Single<List<Test>>
    fun getUserProfile(): Observable<User>
}

class RepositoryImpl constructor(
    private val preferences: AppPreferences,
    private val remoteService: RemoteService,
    private val prefsCookie: AppPreferencesCookie,
    private val databaseService: AppDatabaseService
) : Repository {

    override fun doLoginUserApiCall(request: LoginRequest): Single<User> {
        return remoteService.doLoginUserApiCall(request)
    }

    override fun getUserApiCall(): Observable<UserResponse> {
        return remoteService.getUserApiCall()
            .concatMap { saveUser(it) }
    }

    override fun getHomeworksApiCall(): Single<HomeworksResponse> {
        return remoteService.getHomeworksApiCall()
    }

    override fun getStatusTestApiCall(id: String): Single<TestStatusResponse> {
        return remoteService.getStatusTestApiCall(id)
    }

    override fun getQuestionsApiCall(id: String): Single<List<TestQuestionResponse>> {
        return remoteService.getQuestionsApiCall(id)
    }

    override fun getQuestionApiCall(id: String, problemId: String): Single<TestQuestionResponse> {
        return remoteService.getQuestionApiCall(id, problemId)
    }

    override fun getTests(): Single<List<Test>> {
        return getHomeworksApiCall()
            .toObservable()
            .flatMapIterable { it.homeworks }
            .flatMapIterable { it.tasks }
            .map { it.test }
            .filter { it.taskType == "test_during_lecture" }
            .toList()
    }

    override fun setUserAsLoggedOut() {
        clearCookies()
        clearUserFromDatabase()
        setCurrentUserLoggedInMode(LoggedInMode.LOGGED_IN_MODE_LOGGED_OUT)
    }

    override fun setCurrentUserLoggedInMode(mode: LoggedInMode) {
        preferences.setCurrentUserLoggedInMode(mode)
    }

    override fun getCurrentUserLoggedInMode(): Int {
        return preferences.getCurrentUserLoggedInMode()
    }

    override fun isAuthenticated(): Boolean {
        return preferences.isAuthenticated()
    }

    override fun doLogoutUserApiCall(xcsrftoken: String?): Single<Response<Void>> {
        return remoteService.doLogoutUserApiCall(xcsrftoken)
    }

    override fun getUserProfile(): Observable<User> {
        return Observable.concat(
            getUserApiCall().map { it.user }.onErrorResumeNext(Observable.empty()),
            getUser()
        )
    }

    override fun doSendAnswerApiCall(
        lectureId: String,
        questionId: String,
        testRequest: TestRequest,
        xcsrftoken: String?
    ): Single<AnswerResponse> {
        return remoteService.doSendAnswerApiCall(lectureId, questionId, testRequest, xcsrftoken)
    }

    override fun saveUser(userResponse: UserResponse): Observable<UserResponse> {
        return databaseService.saveUser(userResponse)
    }

    override fun getUser(): Observable<User> {
        return databaseService.getUser()
    }

    override fun clearUserFromDatabase() {
        databaseService.clearUserFromDatabase()
    }

    override fun updateUserApiCall(user: User, xcsrftoken: String?): Single<String> {
        return remoteService.updateUserApiCall(user, xcsrftoken)
    }

    override fun setCookie(cookie: String?) {
        prefsCookie.setCookie(cookie)
    }

    override fun getCookie(): String? {
        return prefsCookie.getCookie()
    }

    override fun setToken(token: String?) {
        prefsCookie.setToken(token)
    }

    override fun getToken(): String? {
        return prefsCookie.getToken()
    }

    override fun clearCookies() {
        prefsCookie.clearCookies()
    }
}