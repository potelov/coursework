package ru.fintech.coursework.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Problem(

    @SerializedName("id")
    @Expose
    var id: Int,
    @SerializedName("cms_page")
    @Expose
    var cmsPage: CmsPage,
    @SerializedName("verbose_title")
    @Expose
    var verboseTitle: String?,
    @SerializedName("problem_type")
    @Expose
    var problemType: String,
    @SerializedName("answer_choices")
    @Expose
    var answerChoices: List<String>,
    @SerializedName("attempts_limit_type")
    @Expose
    var attemptsLimitType: String?,
    @SerializedName("ML")
    @Expose
    var ml: String?,
    @SerializedName("TL")
    @Expose
    var tl: String?,
    @SerializedName("problem_page_data")
    @Expose
    var problemPageData: String?,
    @SerializedName("allowed_extensions")
    @Expose
    var allowedExtensions: String?,
    @SerializedName("max_submission_size")
    @Expose
    var maxSubmissionSize: Int,
    @SerializedName("allowed_languages")
    @Expose
    var allowedLanguages: List<String>?
) : Parcelable
