package ru.fintech.coursework.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Tasks(

    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("task")
    @Expose
    val test: Test,
    @SerializedName("status")
    @Expose
    val status: String,
    @SerializedName("mark")
    @Expose
    val mark: String

)
