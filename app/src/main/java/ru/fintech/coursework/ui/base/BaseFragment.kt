package ru.fintech.coursework.ui.base

import android.content.Context
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import ru.fintech.coursework.R

abstract class BaseFragment : Fragment(), MvpView {

    private var activity: BaseActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is BaseActivity) {
            this.activity = context
        }
    }

    override fun onDetach() {
        activity = null
        super.onDetach()
    }

    override fun onError(message: String) {
        activity?.run { onError(message) }
    }

    override fun onError(@StringRes resId: Int) {
        activity?.run { onError(resId) }
    }

    override fun showMessage(@StringRes resId: Int) {
        activity?.run { showMessage(resId) }
    }

    override fun showMessage(message: String) {
        activity?.run { showMessage(message) }
    }

    override fun showLoading() {
        activity?.run { showLoading() }
    }

    override fun hideLoading() {
        activity?.run { hideLoading() }
    }

    override fun onPause() {
        super.onPause()
        hideLoading()
    }

    override fun replaceFragment(fragment: Fragment) {
        val transaction = childFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
        transaction.addToBackStack(fragment.javaClass.name)
        transaction.commit()
    }

    private fun handleBackPressed(manager: FragmentManager): Boolean {
        for (frag in manager.fragments) {
            if (frag == null) continue
            if (frag.isVisible && frag is BaseFragment) {
                if (frag.onBackPressed()) {
                    return true
                }
            }
        }
        return false
    }

    fun onBackPressed(): Boolean {
        val childFragmentManager = childFragmentManager
        if (handleBackPressed(childFragmentManager)) {
            return true
        } else if (userVisibleHint && childFragmentManager.backStackEntryCount > 1) {
            childFragmentManager.popBackStack()
            return true
        }
        return false
    }

    fun popBackStack() {
        childFragmentManager.popBackStack()
    }

    override fun getContext(): BaseActivity? {
        return activity
    }

    fun getParent(): BaseFragment {
        return parentFragment as BaseFragment
    }

    override fun openActivityOnCookieExpire() {
        activity?.run { openActivityOnCookieExpire() }
    }
}
