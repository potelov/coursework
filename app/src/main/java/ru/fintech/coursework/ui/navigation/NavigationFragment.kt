package ru.fintech.coursework.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.fragment_navigation.*
import ru.fintech.coursework.R
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseFragment
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.ui.profile.ProfileFragment
import ru.fintech.coursework.ui.settings.SettingsFragment
import ru.fintech.coursework.ui.study.StudyFragment

interface NavigationMvpView : MvpView {
    fun showStudyFragment()
    fun showProfileFragment()
    fun showSettingsFragment()
    fun setSelectedTab(selectedTab: Int)
}

class NavigationFragment : BaseFragment(), NavigationMvpView {

    private val presenter by lazy { providePresenter() }

    companion object {
        fun newInstance(): NavigationFragment {
            val args = Bundle()
            val fragment = NavigationFragment()
            fragment.retainInstance = true
            fragment.arguments = args
            return fragment
        }

        private val NAVIGATION_PAGES = arrayOf(
            { StudyFragment.newInstance() },
            { ProfileFragment.newInstance() },
            { SettingsFragment.newInstance() }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_navigation, container, false)
        setUp()
        return view
    }

    private fun setUp() {
        context?.findViewById<BottomNavigationView>(R.id.navigation)?.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_profile -> presenter.onProfileClick()
                R.id.navigation_settings -> presenter.onSettingsClick()
                R.id.navigation_study -> presenter.onStudyClick()
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewPager()
        presenter.init()
    }

    private fun setupViewPager() {
        viewPager.adapter = NavigationAdapter(childFragmentManager)
        viewPager.offscreenPageLimit = 3
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun showStudyFragment() {
        viewPager.setCurrentItem(0, false)
    }

    override fun showProfileFragment() {
        viewPager.setCurrentItem(1, false)
    }

    override fun showSettingsFragment() {
        viewPager.setCurrentItem(2, false)
    }

    override fun setSelectedTab(selectedTab: Int) {
        context?.findViewById<BottomNavigationView>(R.id.navigation)?.selectedItemId = selectedTab
    }

    inner class NavigationAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        override fun getCount() = NAVIGATION_PAGES.size
        override fun getItem(position: Int) = NAVIGATION_PAGES[position]()
    }
}