package ru.fintech.coursework.ui.navigation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ru.fintech.coursework.R
import ru.fintech.coursework.ui.base.BaseFragment

class NavigationContainerFragment : BaseFragment() {

    companion object {
        fun newInstance(): NavigationContainerFragment {
            val args = Bundle()
            val fragment = NavigationContainerFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_container, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (savedInstanceState == null) {
            replaceFragment(NavigationFragment.newInstance())
        }
    }
}