package ru.fintech.coursework.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.*
import ru.fintech.coursework.R
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseActivity
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.ui.main.MainActivity

interface LoginMvpView : MvpView {
    fun openMainActivity()
    fun disableButton()
    fun enableButton()
}

class LoginActivity : BaseActivity(), LoginMvpView {

    private val presenter by lazy { providePresenter() }

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        presenter.onAttach(this)
        setUp()
    }

    private fun setUp() {
        btn_login.setOnClickListener {
            presenter.onLoginClick(
                et_email.text.toString().trim(),
                et_password.text.toString().trim()
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun openMainActivity() {
        startActivity(MainActivity.getStartIntent(this))
        finish()
    }

    override fun disableButton() {
        btn_login.isEnabled = false
    }

    override fun enableButton() {
        btn_login.isEnabled = true
    }
}