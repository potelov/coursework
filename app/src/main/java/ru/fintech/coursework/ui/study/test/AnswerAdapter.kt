package ru.fintech.coursework.ui.study.test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_multiple_answer.view.*
import kotlinx.android.synthetic.main.item_single_answer.view.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.LastSubmission
import ru.fintech.coursework.ui.base.BaseViewHolder

private const val SELECT_ONE = "SELECT_ONE"

class AnswerAdapter(
    private val items: List<String>,
    private val type: String,
    private val lastSubmission: LastSubmission?,
    private val callbacks: Callbacks

) : RecyclerView.Adapter<BaseViewHolder>() {

    private var lastCheckedPosition = -1

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): BaseViewHolder {
        val singleView = LayoutInflater.from(
            viewGroup.context).inflate(R.layout.item_single_answer, viewGroup, false)
        val multipleView = LayoutInflater.from(
            viewGroup.context).inflate(R.layout.item_multiple_answer, viewGroup, false)
        return when (type) {
            SELECT_ONE -> SingleViewHolder(singleView)
            else -> MultipleViewHolder(multipleView)
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class MultipleViewHolder(private val view: View) : BaseViewHolder(view) {

        override fun onBind(position: Int) {
            val string = items[position]
            view.chk_answer.text = string

            lastSubmission?.file.run {
                val char = lastSubmission?.file!![adapterPosition]
                view.chk_answer.isChecked = char == '1'
            }

            view.chk_answer.setOnClickListener {
                callbacks.onMultipleAnswerClicked(adapterPosition, view.chk_answer.isChecked)
            }
        }
    }

    inner class SingleViewHolder(private val view: View) : BaseViewHolder(view) {
        override fun onBind(position: Int) {
            val string = items[position]
            view.rb_answer.text = string

            if (lastSubmission?.file != null && lastCheckedPosition == -1) {
                val char = lastSubmission.file!![adapterPosition]
                view.rb_answer.isChecked = char == '1'
                if (view.rb_answer.isChecked) {
                    lastCheckedPosition = adapterPosition
                }
            } else {
                view.rb_answer.isChecked = lastCheckedPosition == adapterPosition
            }

            view.rb_answer.setOnClickListener {
                lastCheckedPosition = adapterPosition
                callbacks.onSingleAnswerClicked(adapterPosition)
                notifyDataSetChanged()
            }
        }
    }
}


