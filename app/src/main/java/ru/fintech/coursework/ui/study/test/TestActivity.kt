package ru.fintech.coursework.ui.study.test

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_test.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.Test
import ru.fintech.coursework.data.model.TestQuestionResponse
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseActivity
import ru.fintech.coursework.ui.base.MvpView

interface TestMvpView : MvpView {
    fun updateView(questions: List<TestQuestionResponse>)
    fun updateTitle(title: String, lectureName: String)
    fun showNextQuestion(pageNumber: Int)
    fun showCompleteDialog()
}

class TestActivity : BaseActivity(), TestMvpView {

    private val presenter by lazy { providePresenter() }
    private lateinit var questionAdapter: QuestionAdapter

    companion object {
        private const val EXTRA_TEST = "EXTRA_TEST"
        fun getStartIntent(context: Context, test: Test): Intent {
            val intent = Intent(context, TestActivity::class.java)
            intent.putExtra(EXTRA_TEST, test)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        presenter.onAttach(this)
        questionAdapter = QuestionAdapter()
        setUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun updateView(questions: List<TestQuestionResponse>) {
        questionAdapter.setItems(questions)
    }

    private fun setUp() {
        setupPagerAdapter()
        val test = intent.getParcelableExtra(EXTRA_TEST) as Test
        iv_back.setOnClickListener { onBackPressed() }
        btn_continue.setOnClickListener { presenter.onContinueClick(test, viewPager.currentItem) }
        btn_skip.setOnClickListener { presenter.onSkipClick(viewPager.currentItem) }
        presenter.init(test)
    }

    private fun setupPagerAdapter() {
        viewPager.adapter = questionAdapter
        viewPager.offscreenPageLimit = 3
        viewPager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                updateButtonName()
            }
        })
    }

    override fun updateTitle(title: String, lectureName: String) {
        tv_title.text = title
        tv_name.text = lectureName
    }

    override fun showNextQuestion(pageNumber: Int) {
        viewPager.setCurrentItem(pageNumber, false)
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager.setCurrentItem(viewPager.currentItem - 1, false)
        }
    }

    private fun openStudyFragment() {
        finish()
    }

    private fun updateButtonName() {
        if (viewPager.currentItem == questionAdapter.realCount - 1) {
            btn_continue.text = getString(R.string.test_btn_label_complete)
        } else {
            btn_continue.text = getString(R.string.test_btn_label_continue)
        }
    }

    override fun showCompleteDialog() {
        val alert = AlertDialog.Builder(this)
        alert.setCancelable(false)
        alert.setTitle(getString(R.string.app_name))
        alert.setMessage(getString(R.string.test_complete))
        alert.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
            openStudyFragment()
        }
        alert.show()
    }

}