package ru.fintech.coursework.ui.navigation

import ru.fintech.coursework.R
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter

interface NavigationMvpPresenter<V : NavigationMvpView> : MvpPresenter<V> {
    fun init()
    fun onStudyClick()
    fun onProfileClick()
    fun onSettingsClick()
}

class NavigationPresenter<V : NavigationMvpView> : BasePresenter<V>(), NavigationMvpPresenter<V> {

    private var selectedTab = R.id.navigation_profile

    override fun init() {
        getView()?.setSelectedTab(selectedTab)
    }

    override fun onStudyClick() {
        selectedTab = R.id.navigation_study
        getView()?.showStudyFragment()
    }

    override fun onProfileClick() {
        selectedTab = R.id.navigation_profile
        getView()?.showProfileFragment()
    }

    override fun onSettingsClick() {
        selectedTab = R.id.navigation_settings
        getView()?.showSettingsFragment()
    }
}