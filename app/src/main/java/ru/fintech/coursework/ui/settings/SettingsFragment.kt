package ru.fintech.coursework.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_settings.*
import ru.fintech.coursework.R
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseFragment
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.ui.login.LoginActivity

interface SettingsMvpView : MvpView {
    fun openLoginActivity()
}

class SettingsFragment : BaseFragment(), SettingsMvpView {

    private val presenter by lazy { providePresenter() }

    companion object {
        fun newInstance(): SettingsFragment {
            val args = Bundle()
            val fragment = SettingsFragment()
            fragment.retainInstance = true
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        logout.setOnClickListener { showDialog() }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun openLoginActivity() {
        activity?.run { startActivity(LoginActivity.getStartIntent(activity!!)) }
        activity?.finish()
    }

    private fun showDialog() {
        activity?.run {
            val alert = AlertDialog.Builder(activity!!)
            alert.setTitle(getString(R.string.app_name))
            alert.setMessage(getString(R.string.settings_confirm_exit))
            alert.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                presenter.onLogoutClick()
            }
            alert.setNegativeButton(getString(android.R.string.cancel)) { dialog, _ ->
                dialog.dismiss()
            }
            alert.show()
        }
    }
}