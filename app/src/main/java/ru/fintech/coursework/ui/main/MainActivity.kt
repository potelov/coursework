package ru.fintech.coursework.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import ru.fintech.coursework.R
import ru.fintech.coursework.ui.base.BaseActivity
import ru.fintech.coursework.ui.navigation.NavigationContainerFragment

class MainActivity : BaseActivity() {

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            replaceFragment(NavigationContainerFragment.newInstance())
        }
    }
}