package ru.fintech.coursework.ui.login

import ru.fintech.coursework.R
import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.data.model.LoginRequest
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter
import ru.fintech.coursework.utils.LoggedInMode
import timber.log.Timber

interface LoginMvpPresenter<V : LoginMvpView> : MvpPresenter<V> {
    fun onLoginClick(email: String, password: String)
}

class LoginPresenter<V : LoginMvpView>(
    private val repository: Repository)
    : BasePresenter<V>(), LoginMvpPresenter<V> {

    override fun onLoginClick(email: String, password: String) {
        when {
            email.isEmpty() -> getView()?.onError(R.string.error_email_empty)
            password.isEmpty() -> getView()?.onError(R.string.error_password_empty)
            else -> {
                getView()?.showLoading()
                getView()?.disableButton()
                compositeDisposable.add(
                    repository
                        .doLoginUserApiCall(LoginRequest(email, password))
                        .subscribeOn(schedulerProvider.io())
                        .observeOn(schedulerProvider.ui())
                        .subscribe({
                            getView()?.hideLoading()
                            getView()?.enableButton()
                            getView()?.openMainActivity()
                            setUserIsLoggedIn()
                        }, { throwable ->
                            getView()?.hideLoading()
                            getView()?.enableButton()
                            if (isOffline(throwable)) {
                                getView()?.onError(R.string.error_no_connection)
                            } else {
                                getView()?.onError(R.string.error_failed_login_attempt)
                            }
                            Timber.d(throwable)
                        })
                )
            }
        }
    }

    private fun setUserIsLoggedIn() {
        repository.setCurrentUserLoggedInMode(LoggedInMode.LOGGED_IN_MODE_SERVER)
    }
}