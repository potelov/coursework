package ru.fintech.coursework.ui.base

import io.reactivex.disposables.CompositeDisposable
import retrofit2.HttpException
import ru.fintech.coursework.R
import ru.fintech.coursework.utils.rx.AppSchedulerProvider
import ru.fintech.coursework.utils.rx.SchedulerProvider
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.net.ssl.HttpsURLConnection

abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    val schedulerProvider: SchedulerProvider = AppSchedulerProvider()
    val compositeDisposable = CompositeDisposable()

    private var mvpView: V? = null

    fun getView(): V? = mvpView

    override fun onAttach(mvpView: V) {
        this.mvpView = mvpView
    }

    override fun onDetach() {
        compositeDisposable.dispose()
        mvpView = null
    }

    override fun handleError(error: Throwable) {
        if (error is HttpException) {
            when (error.code()) {
                HttpsURLConnection.HTTP_FORBIDDEN -> {
                    getView()?.showMessage(R.string.error_login_repeat)
                    getView()?.openActivityOnCookieExpire()
                }
            }
        }
        if (isOffline(error)) {
            getView()?.showMessage(R.string.error_no_connection)
        }
    }

    fun isOffline(throwable: Throwable?): Boolean {
        return (throwable is UnknownHostException
                || throwable is SocketException
                || throwable is SocketTimeoutException)
    }
}