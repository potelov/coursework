package ru.fintech.coursework.ui.splash

import android.os.Bundle
import ru.fintech.coursework.R
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseActivity
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.ui.login.LoginActivity
import ru.fintech.coursework.ui.main.MainActivity

interface SplashMvpView : MvpView {
    fun openMainActivity()
    fun openLoginActivity()
}

class SplashActivity : BaseActivity(), SplashMvpView {

    private val presenter by lazy { providePresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter.onAttach(this)
        setUp()
    }

    private fun setUp() {
        presenter.init()
    }

    override fun openMainActivity() {
        startActivity(MainActivity.getStartIntent(this))
        finish()
    }

    override fun openLoginActivity() {
        startActivity(LoginActivity.getStartIntent(this))
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }
}