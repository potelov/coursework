package ru.fintech.coursework.ui.study.test

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_question.view.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.LastSubmission
import ru.fintech.coursework.data.model.TestQuestionResponse
import ru.fintech.coursework.ui.custom.RecyclerPagerAdapter
import ru.fintech.coursework.utils.CommonUtil
import timber.log.Timber

interface Callbacks {
    fun onSingleAnswerClicked(position: Int)
    fun onMultipleAnswerClicked(position: Int, checked: Boolean)
}

class QuestionAdapter : RecyclerPagerAdapter<TestQuestionResponse, QuestionAdapter.QuestionHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): QuestionHolder {
        val view = LayoutInflater.from(parent?.context)
            .inflate(R.layout.item_question, parent, false)
        return QuestionHolder(view)
    }

    override fun onBindViewHolder(viewHolder: QuestionHolder?, position: Int) {
        viewHolder?.render()
    }

    override fun onNotifyItemChanged(viewHolder: QuestionHolder?) {
        viewHolder?.render()
    }

    inner class QuestionHolder(itemView: View) : RecyclerPagerAdapter<*,*>.ViewHolder(itemView) {

        override fun render() {
            val question = getItemAt(adapterPosition)
            itemView.tv_question.text = CommonUtil.fromHtml(question.problem.cmsPage.unstyledStatement)
            itemView.recycler.adapter = AnswerAdapter(
                question.problem.answerChoices,
                question.problem.problemType,
                question.lastSubmission,
                callback)
        }

        private var callback: Callbacks = object : Callbacks {
            override fun onSingleAnswerClicked(position: Int) {
                val question = getItemAt(adapterPosition)
                if (question.lastSubmission == null) {
                    question.lastSubmission = LastSubmission()
                }
                question.lastSubmission?.setSingleFile(position, question.problem.answerChoices.size)
            }

            override fun onMultipleAnswerClicked(position: Int, checked: Boolean) {
                val question = getItemAt(adapterPosition)
                if (question.lastSubmission == null) {
                    question.lastSubmission = LastSubmission()
                }
                question.lastSubmission?.setMultipleFile(position, question.problem.answerChoices.size, checked)
            }
        }
    }
}
