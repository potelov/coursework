package ru.fintech.coursework.ui.profile

import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.data.model.User
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter
import timber.log.Timber

interface ProfileMvpPresenter<V : ProfileMvpView> : MvpPresenter<V> {
    fun init()
    fun getUser()
    fun updateUser(user: User)
}

class ProfilePresenter<V : ProfileMvpView> constructor(
    private val repository: Repository) : BasePresenter<V>(),
    ProfileMvpPresenter<V> {

    override fun init() {
        getView()?.showLoading()
        compositeDisposable.add(
            repository
                .getUserProfile()
                .filter { it.id > 0 }
                .first(User(0))
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.hideLoading()
                    getView()?.updateView(it)
                }, { throwable ->
                    getView()?.hideLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }

    override fun getUser() {
        compositeDisposable.add(
            repository
                .getUserApiCall()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.updateView(it.user)
                    getView()?.hideSwipeRefreshLoading()
                }, { throwable ->
                    getView()?.hideSwipeRefreshLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }

    override fun updateUser(user: User) {
        getView()?.showLoading()
        val xcsrftoken = repository.getToken()
        compositeDisposable.add(
            repository
                .updateUserApiCall(user, xcsrftoken)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.hideLoading()
                    getView()?.onError(it)
                }, { throwable ->
                    getView()?.hideLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }
}
