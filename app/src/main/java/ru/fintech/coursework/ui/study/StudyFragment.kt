package ru.fintech.coursework.ui.study

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.fragment_study.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.Test
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseFragment
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.ui.study.test.TestActivity

interface StudyMvpView : MvpView {
    fun updateView(tests: List<Test>)
    fun openTestActivity(test: Test)
    fun showDialog(test: Test)
}

interface Callbacks {
    fun onTestClicked(test: Test)
}

class StudyFragment : BaseFragment(), StudyMvpView {

    private val presenter by lazy { providePresenter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    companion object {
        fun newInstance(): StudyFragment {
            val args = Bundle()
            val fragment = StudyFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_study, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter.init()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun openTestActivity(test: Test) {
        activity?.run { startActivity(TestActivity.getStartIntent(activity!!, test)) }
    }

    override fun updateView(tests: List<Test>) {
        recycler.adapter = StudyAdapter(tests, callback)
        checkForOngoingTest(tests)
    }

    private fun checkForOngoingTest(tests: List<Test>) {
        tests.forEachIndexed { index, test ->
            if (test.contestInfo?.contestStatus?.status == "ongoing") {
                recycler.scrollToPosition(index)
                return@forEachIndexed
            }
        }
    }

    private var callback: Callbacks = object : Callbacks {
        override fun onTestClicked(test: Test) {
            presenter.onTestClick(test)
        }
    }

    override fun showDialog(test: Test) {
        activity?.run {
            val alert = AlertDialog.Builder(activity!!)
            alert.setTitle(getString(R.string.app_name))
            alert.setMessage(getString(R.string.test_start))
            alert.setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                openTestActivity(test)
            }
            alert.setNegativeButton(getString(android.R.string.cancel)) { dialog, _ ->
                dialog.dismiss()
            }
            alert.show()
        }
    }
}