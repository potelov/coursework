package ru.fintech.coursework.ui.study.test

import ru.fintech.coursework.R
import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.data.model.Test
import ru.fintech.coursework.data.model.TestQuestionResponse
import ru.fintech.coursework.data.model.TestRequest
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter
import timber.log.Timber

interface TestMvpPresenter<V : TestMvpView> : MvpPresenter<V> {
    fun init(test: Test)
    fun onSkipClick(currentQuestion: Int)
    fun onContinueClick(test: Test, currentQuestion: Int)
}

class TestPresenter<V : TestMvpView> constructor(
    private val repository: Repository
) : BasePresenter<V>(), TestMvpPresenter<V> {

    private lateinit var questions: List<TestQuestionResponse>

    override fun init(test: Test) {
        getView()?.updateTitle(test.shortName, test.title)
        getView()?.showLoading()
        compositeDisposable.add(
            repository
                .getQuestionsApiCall(test.contestInfo.contestUrl)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    questions = it
                    getView()?.updateView(questions)
                    getView()?.hideLoading()
                }, { throwable ->
                    getView()?.hideLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )    }

    override fun onSkipClick(currentQuestion: Int) {
        getView()?.showNextQuestion(currentQuestion + 1)
    }

    override fun onContinueClick(test: Test, currentQuestion: Int) {
        val file = questions[currentQuestion].lastSubmission?.file
        val xcsrftoken = repository.getToken()
        val pageNumber = currentQuestion + 1

        getView()?.showLoading()
        compositeDisposable.add(
            repository
                .doSendAnswerApiCall(
                    test.contestInfo.contestUrl,
                    pageNumber.toString(),
                    TestRequest(file!!),
                    xcsrftoken
                )
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.hideLoading()
                    getView()?.showMessage(R.string.test_answer_accepted)
                    if (currentQuestion == questions.size - 1) {
                        getView()?.showCompleteDialog()
                    } else {
                        getView()?.showNextQuestion(currentQuestion + 1)
                    }
                }, { throwable ->
                    getView()?.hideLoading()
                    getView()?.showMessage(R.string.test_answer_not_accepted)
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }
}