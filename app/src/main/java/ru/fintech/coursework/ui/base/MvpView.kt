package ru.fintech.coursework.ui.base

import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

interface MvpView {

    fun replaceFragment(fragment: Fragment)

    fun onError(@StringRes resId: Int)

    fun onError(message: String)

    fun openActivityOnCookieExpire()

    fun showLoading()

    fun hideLoading()

    fun showMessage(@StringRes resId: Int)

    fun showMessage(message: String)
}
