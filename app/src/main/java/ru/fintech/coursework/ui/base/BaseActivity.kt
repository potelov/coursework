package ru.fintech.coursework.ui.base

import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.snackbar.Snackbar
import ru.fintech.coursework.R
import ru.fintech.coursework.di.provideRepository
import ru.fintech.coursework.ui.login.LoginActivity

abstract class BaseActivity : AppCompatActivity(), MvpView {

    private val repository by lazy { provideRepository() }

    override fun showLoading() {
        findViewById<View>(R.id.progress_bar).visibility = View.VISIBLE
    }

    override fun hideLoading() {
        findViewById<View>(R.id.progress_bar).visibility = View.GONE
    }

    override fun onError(@StringRes resId: Int) {
        onError(getString(resId))
    }

    override fun onError(message: String) {
        showSnackBar(message)
    }

    override fun showMessage(resId: Int) {
        showMessage(getString(resId))
    }

    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    private fun showSnackBar(message: String) {
        val snackbar = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT)
        val sbView = snackbar.view
        val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
        textView.setTextColor(ContextCompat.getColor(this, R.color.white))
        snackbar.show()
    }


    override fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.addToBackStack(fragment.javaClass.name)
        fragmentTransaction.replace(R.id.fragment_container, fragment, fragment.javaClass.name)
            .commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        if (!handleBackPressed(supportFragmentManager)) {
            if (supportFragmentManager.backStackEntryCount > 1) {
                super.onBackPressed()
            } else {
                finish()
            }
        }
    }

    private fun handleBackPressed(manager: FragmentManager): Boolean {
        for (frag in manager.fragments) {
            if (frag == null) continue
            if (frag.isVisible && frag is BaseFragment) {
                if (frag.onBackPressed()) {
                    return true
                }
            }
        }
        return false
    }

    override fun openActivityOnCookieExpire() {
        repository.setUserAsLoggedOut()
        startActivity(LoginActivity.getStartIntent(this))
        finish()
    }
}
