package ru.fintech.coursework.ui.settings

import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter
import timber.log.Timber

interface SettingsMvpPresenter<V : SettingsMvpView> : MvpPresenter<V> {
    fun onLogoutClick()
}

class SettingsPresenter<V : SettingsMvpView>(
    private val repository: Repository) : BasePresenter<V>(), SettingsMvpPresenter<V> {

    override fun onLogoutClick() {
        getView()?.showLoading()
        val xcsrftoken = repository.getToken()
        compositeDisposable.add(
            repository
                .doLogoutUserApiCall(xcsrftoken)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    setUserAsLoggedOut()
                    getView()?.openLoginActivity()
                    getView()?.hideLoading()
                }, { throwable ->
                    getView()?.hideLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }

    private fun setUserAsLoggedOut() {
        repository.setUserAsLoggedOut()
    }
}