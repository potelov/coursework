package ru.fintech.coursework.ui.profile

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.fragment_profile.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.User
import ru.fintech.coursework.di.providePresenter
import ru.fintech.coursework.ui.base.BaseFragment
import ru.fintech.coursework.ui.base.MvpView
import ru.fintech.coursework.utils.FINTECH_URL


interface ProfileMvpView : MvpView {
    fun updateView(user: User)
    fun hideSwipeRefreshLoading()
}

class ProfileFragment : BaseFragment(), ProfileMvpView {

    private val presenter by lazy { providePresenter() }
    private lateinit var user: User

    companion object {
        fun newInstance(): ProfileFragment {
            val args = Bundle()
            val fragment = ProfileFragment()
            fragment.retainInstance = true
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAttach(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    private fun setUp() {
        presenter.init()
        setupTextWatcher()
        swipeRefresh.setOnRefreshListener { presenter.getUser() }
        btn_change.setOnClickListener { presenter.updateUser(getCurrentUser()) }
    }

    private fun setupTextWatcher() {
        et_region.addTextChangedListener(textWatcher)
        et_lastname.addTextChangedListener(textWatcher)
        et_birthday.addTextChangedListener(textWatcher)
        et_firstname.addTextChangedListener(textWatcher)
        et_university.addTextChangedListener(textWatcher)
        et_email_address.addTextChangedListener(textWatcher)
    }

    private fun activateButton(active: Boolean) {
        btn_change.isEnabled = active
    }

    override fun hideSwipeRefreshLoading() {
        if (swipeRefresh.isRefreshing) {
            swipeRefresh.isRefreshing = false
        }
    }

    override fun updateView(user: User) {
        user.region?.run { et_region.setText(user.region) }
        user.email?.run { et_email_address.setText(user.email) }
        user.birthday?.run { et_birthday.setText(user.birthday) }
        user.lastName?.run { et_lastname.setText(user.lastName) }
        user.firstName?.run { et_firstname.setText(user.firstName) }
        user.university?.run { et_university.setText(user.university) }

        Glide.with(this)
            .load(FINTECH_URL + user.avatar)
            .apply(RequestOptions.circleCropTransform().error(R.drawable.ic_user_ava))
            .into(iv_profile_pic)

        activateButton(false)
        this.user = user
    }

    private var textWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            //do nothing
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            activateButton(true)
        }

        override fun afterTextChanged(s: Editable) {
            // do nothing
        }
    }

    private fun getCurrentUser(): User {
        return this.user.apply {
            university = et_university.text.toString()
            firstName = et_firstname.text.toString()
            email = et_email_address.text.toString()
            lastName = et_lastname.text.toString()
            birthday = et_birthday.text.toString()
            region = et_region.text.toString()
        }
    }
}