package ru.fintech.coursework.ui.study

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_study.view.*
import ru.fintech.coursework.R
import ru.fintech.coursework.data.model.Test

class StudyAdapter(
    private val items: List<Test>,
    private val callbacks: Callbacks
) : RecyclerView.Adapter<StudyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(
            viewGroup.context
        ).inflate(R.layout.item_study, viewGroup, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

        fun onBind(test: Test) {
            view.tv_name.text = """ТЗ.$adapterPosition""" //todo fix when short name is correct

            if (test.contestInfo.contestStatus?.status == "ongoing") {
                view.view_badge.visibility = View.VISIBLE
            } else {
                view.view_badge.visibility = View.GONE
            }
            view.setOnClickListener { callbacks.onTestClicked(test) }
        }
    }
}
