package ru.fintech.coursework.ui.splash

import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter

interface SplashMvpPresenter<V : SplashMvpView> : MvpPresenter<V> {
    fun init()
}

class SplashPresenter<V : SplashMvpView>(private val repository: Repository) : BasePresenter<V>(),
    SplashMvpPresenter<V> {

    override fun init() {
        val isUserAuthenticated = repository.isAuthenticated()
        decideNextActivity(isUserAuthenticated)
    }

    private fun decideNextActivity(isUserAuthenticated: Boolean) {
        if (isUserAuthenticated) {
            getView()?.openMainActivity()
        } else {
            getView()?.openLoginActivity()
        }
    }
}