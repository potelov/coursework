package ru.fintech.coursework.ui.custom;

import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class RecyclerPagerAdapter<M, V extends RecyclerPagerAdapter.ViewHolder>
        extends PagerAdapter {

    private List<M> items = new ArrayList<>();

    private boolean looping;

    private ViewPager pager;

    public void setLooping(boolean state) {
        this.looping = state;
    }

    public M getItemAt(int position) {
        return items.get(position);
    }

    public void setItems(List<M> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnlyItems(List<M> items) {
        this.items = items;
    }

    public abstract V onCreateViewHolder(ViewGroup parent, int viewType);

    public abstract void onBindViewHolder(V viewHolder, int position);

    private final Map<Integer, RecycleCache> mRecycleCacheMap = new HashMap<>();



    @Override
    public int getCount() {
        if (items.size() == 0) return 0;
        return looping ? Integer.MAX_VALUE : getRealCount();
    }


    public int getRealCount() {
        return items.size();
    }

    public int getVirtualPosition(int position) {
        if (items.size() == 0) return 0;
        return looping ? position % items.size() : position;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup parent, int position) {
        if (pager == null) this.pager = (ViewPager) parent;
        int realPosition = getVirtualPosition(position);
        int viewType = getItemViewType(realPosition);
        if (!mRecycleCacheMap.containsKey(viewType)) {
            mRecycleCacheMap.put(viewType, new RecycleCache(viewType));
        }
        V viewHolder = mRecycleCacheMap.get(viewType).getFreeViewHolder();
        viewHolder.setAttached(true);
        viewHolder.setAdapterPosition(realPosition);
        onBindViewHolder(viewHolder, realPosition);
        parent.addView(viewHolder.itemView);
        return viewHolder;
    }

    @Override
    public void destroyItem(ViewGroup parent, int position, Object object) {
        if (object instanceof RecyclerPagerAdapter.ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) object;
            viewHolder.attached = false;
            viewHolder.adapterPosition = getVirtualPosition(position);
            parent.removeView(viewHolder.itemView);
        }
    }

    public int getCurrentItem() {
        return getVirtualPosition(pager.getCurrentItem());
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object.equals(view) || isViewHolderFromObject(view, object);
    }

    private boolean isViewHolderFromObject(View view, Object object) {
        return object instanceof RecyclerPagerAdapter.ViewHolder
                && ((ViewHolder) object).itemView.equals(view);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        for (V viewHolder : getAttachedViewHolders()) {
            onNotifyItemChanged(viewHolder);
            viewHolder.itemView.requestLayout();
        }
    }

    public void update() {
        for (V viewHolder : getAttachedViewHolders()) {
            onNotifyItemChanged(viewHolder);
            viewHolder.itemView.requestLayout();
        }
    }

    public int getItemViewType(int position) {
        return position;
    }


    protected abstract void onNotifyItemChanged(V viewHolder);


    private List<V> getAttachedViewHolders() {
        List<V> viewHolders = new ArrayList<>();
        for (Map.Entry<Integer, RecycleCache> entry : mRecycleCacheMap.entrySet()) {
            List<V> cache = entry.getValue().mCaches;
            int i = 0;
            for (int n = cache.size(); i < n; i++) {
                if (cache.get(i).isAttached()) {
                    viewHolders.add(cache.get(i));
                }
            }
        }
        return viewHolders;
    }

    protected class RecycleCache {


        private final int mViewType;

        private final List<V> mCaches;


        public RecycleCache(int viewType) {
            mViewType = viewType;
            mCaches = new ArrayList<>();
        }

        public V getFreeViewHolder() {
            int i = 0;
            V viewHolder;
            for (int n = mCaches.size(); i < n; i++) {
                viewHolder = mCaches.get(i);
                if (!viewHolder.isAttached()) {
                    return viewHolder;
                }
            }
            viewHolder = RecyclerPagerAdapter.this.onCreateViewHolder(pager, mViewType);
            mCaches.add(viewHolder);
            return viewHolder;
        }
    }


    public abstract class ViewHolder {

        protected final View itemView;

        private int adapterPosition;

        private boolean attached;


        public ViewHolder(View itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.itemView = itemView;
        }

        public boolean isAttached() {
            return attached;
        }

        void setAttached(boolean state) {
            this.attached = state;
        }

        void setAdapterPosition(int position) {
            this.adapterPosition = position;
        }

        public abstract void render();

        protected int getAdapterPosition() {
            return getVirtualPosition(adapterPosition);
        }
    }
}
