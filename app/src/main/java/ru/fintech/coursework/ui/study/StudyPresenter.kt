package ru.fintech.coursework.ui.study

import retrofit2.HttpException
import ru.fintech.coursework.R
import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.data.model.Test
import ru.fintech.coursework.ui.base.BasePresenter
import ru.fintech.coursework.ui.base.MvpPresenter
import timber.log.Timber
import javax.net.ssl.HttpsURLConnection

interface StudyMvpPresenter<V : StudyMvpView> : MvpPresenter<V> {
    fun init()
    fun onTestClick(test: Test)
}

class StudyPresenter<V : StudyMvpView> constructor(
    private val repository: Repository
) : BasePresenter<V>(), StudyMvpPresenter<V> {

    override fun init() {
        getView()?.showLoading()
        compositeDisposable.add(
            repository
                .getTests()
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.hideLoading()
                    getView()?.updateView(it)
                }, { throwable ->
                    getView()?.hideLoading()
                    handleError(throwable)
                    Timber.d(throwable)
                })
        )
    }

    override fun onTestClick(test: Test) {
        val testId = test.contestInfo.contestUrl
        getView()?.showLoading()
        compositeDisposable.add(
            repository
                .getStatusTestApiCall(testId)
                .subscribeOn(schedulerProvider.io())
                .observeOn(schedulerProvider.ui())
                .subscribe({
                    getView()?.hideLoading()
                    getView()?.showDialog(test)
                }, { throwable ->
                    getView()?.hideLoading()
                    Timber.d(throwable)
                    if (throwable is HttpException &&
                        throwable.code() == HttpsURLConnection.HTTP_FORBIDDEN
                    ) {
                        getView()?.onError(R.string.test_not_available)
                    }
                    if (isOffline(throwable)) {
                        getView()?.onError(R.string.error_no_connection)
                    }
                })
        )
    }

}