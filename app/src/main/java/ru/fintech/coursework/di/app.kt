package ru.fintech.coursework.di

import androidx.room.Room
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.fintech.coursework.App
import ru.fintech.coursework.BuildConfig
import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.data.RepositoryImpl
import ru.fintech.coursework.data.database.AppDatabase
import ru.fintech.coursework.data.database.AppDatabaseImpl
import ru.fintech.coursework.data.database.AppDatabaseService
import ru.fintech.coursework.data.preferences.AppPreferences
import ru.fintech.coursework.data.preferences.AppPreferencesCookie
import ru.fintech.coursework.data.preferences.AppPreferencesCookieImpl
import ru.fintech.coursework.data.preferences.AppPreferencesImpl
import ru.fintech.coursework.data.remote.AddCookiesInterceptor
import ru.fintech.coursework.data.remote.ReceivedCookiesInterceptor
import ru.fintech.coursework.data.remote.RemoteService
import ru.fintech.coursework.utils.BASE_URL
import ru.fintech.coursework.utils.DATE_FORMAT
import ru.fintech.coursework.utils.DB_NAME

fun App.provideRepository(): Repository =
    RepositoryImpl(prefs, remoteService, prefsCookie, databaseService)

fun App.providePrefs(): AppPreferences =
    AppPreferencesImpl(this)

fun App.providePrefsCookie(): AppPreferencesCookie =
    AppPreferencesCookieImpl(this)

fun App.provideDatabase(): AppDatabase =
    Room.databaseBuilder(this, AppDatabase::class.java, DB_NAME).build()

fun App.provideDatabaseService(): AppDatabaseService =
    AppDatabaseImpl(database)

fun App.provideRemoteService(): RemoteService =
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(
            OkHttpClient.Builder()
                .addInterceptor(ReceivedCookiesInterceptor(this)) //todo replace on CookieJar??
                .addInterceptor(AddCookiesInterceptor(this))
                .setDebugEnabled(BuildConfig.DEBUG)
                .build()
        )
        .build()
        .create(RemoteService::class.java)

fun provideGson(): Gson =
    GsonBuilder()
        .setDateFormat(DATE_FORMAT)
        .create()

private fun OkHttpClient.Builder.setDebugEnabled(debugEnabled: Boolean) =
    apply {
        if (debugEnabled) {
            addNetworkInterceptor(HttpLoggingInterceptor().also {
                it.level = HttpLoggingInterceptor.Level.BODY
            })
        }
    }


