package ru.fintech.coursework.di

import ru.fintech.coursework.ui.study.StudyFragment
import ru.fintech.coursework.ui.study.StudyMvpPresenter
import ru.fintech.coursework.ui.study.StudyMvpView
import ru.fintech.coursework.ui.study.StudyPresenter
import ru.fintech.coursework.utils.extensions.app

fun StudyFragment.providePresenter(): StudyMvpPresenter<StudyMvpView> {
    return StudyPresenter(app.repository)
}