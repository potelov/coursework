package ru.fintech.coursework.di

import ru.fintech.coursework.ui.study.test.TestActivity
import ru.fintech.coursework.ui.study.test.TestMvpPresenter
import ru.fintech.coursework.ui.study.test.TestMvpView
import ru.fintech.coursework.ui.study.test.TestPresenter
import ru.fintech.coursework.utils.extensions.app

fun TestActivity.providePresenter(): TestMvpPresenter<TestMvpView> {
    return TestPresenter(app.repository)
}
