package ru.fintech.coursework.di

import ru.fintech.coursework.ui.settings.SettingsFragment
import ru.fintech.coursework.ui.settings.SettingsMvpPresenter
import ru.fintech.coursework.ui.settings.SettingsMvpView
import ru.fintech.coursework.ui.settings.SettingsPresenter
import ru.fintech.coursework.utils.extensions.app

fun SettingsFragment.providePresenter(): SettingsMvpPresenter<SettingsMvpView> {
    return SettingsPresenter(app.repository)
}