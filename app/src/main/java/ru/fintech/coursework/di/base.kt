package ru.fintech.coursework.di

import ru.fintech.coursework.data.Repository
import ru.fintech.coursework.ui.base.BaseActivity
import ru.fintech.coursework.utils.extensions.app

fun BaseActivity.provideRepository(): Repository {
    return app.repository
}