package ru.fintech.coursework.di

import ru.fintech.coursework.ui.login.LoginActivity
import ru.fintech.coursework.ui.login.LoginMvpPresenter
import ru.fintech.coursework.ui.login.LoginMvpView
import ru.fintech.coursework.ui.login.LoginPresenter
import ru.fintech.coursework.utils.extensions.app

fun LoginActivity.providePresenter(): LoginMvpPresenter<LoginMvpView> {
    return LoginPresenter(app.repository)
}
