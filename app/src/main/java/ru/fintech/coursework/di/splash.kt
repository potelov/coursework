package ru.fintech.coursework.di

import ru.fintech.coursework.ui.splash.SplashActivity
import ru.fintech.coursework.ui.splash.SplashMvpPresenter
import ru.fintech.coursework.ui.splash.SplashMvpView
import ru.fintech.coursework.ui.splash.SplashPresenter
import ru.fintech.coursework.utils.extensions.app

fun SplashActivity.providePresenter(): SplashMvpPresenter<SplashMvpView> {
    return SplashPresenter(app.repository)
}