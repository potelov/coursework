package ru.fintech.coursework.di

import ru.fintech.coursework.ui.navigation.NavigationMvpPresenter
import ru.fintech.coursework.ui.navigation.NavigationMvpView
import ru.fintech.coursework.ui.navigation.NavigationPresenter

fun providePresenter(): NavigationMvpPresenter<NavigationMvpView> {
    return NavigationPresenter()
}