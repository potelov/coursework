package ru.fintech.coursework.di

import ru.fintech.coursework.ui.profile.ProfileFragment
import ru.fintech.coursework.ui.profile.ProfileMvpPresenter
import ru.fintech.coursework.ui.profile.ProfileMvpView
import ru.fintech.coursework.ui.profile.ProfilePresenter
import ru.fintech.coursework.utils.extensions.app

fun ProfileFragment.providePresenter(): ProfileMvpPresenter<ProfileMvpView> {
    return ProfilePresenter(app.repository)
}